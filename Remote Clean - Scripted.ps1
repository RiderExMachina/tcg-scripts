 $ipfile = "$ENV:userprofile\Desktop\ips.txt"
 $StopWatch = New-Object -TypeName System.Diagnostics.Stopwatch
 $StopWatch.Start()

 function clean {
 	## Change the title
	$host.ui.RawUI.WindowTitle = 'Working on ' + $computer 
	
	net use W: \\$computer\c$ /user:computer\administrator [REDACTED]
	
	echo "Deleting Windows Temp"
	
	cmd.exe /c 'del W:\windows\temp\* /f /s /q'
	
	echo "Deleting User Temp"
	
	cmd.exe /c 'del %remoteuserprofile%\appdata\local\temp\* /f /s /q'
	
	echo "Deleting duplicate downloads"
	
	cmd.exe /c 'del %remoteuserprofile%\downloads\*(*).* /f /s /q'

	net use /d W:
	
	echo "Done!"
	$StopWatch
 }
 if (Test-Path -Path $ipfile) {
	foreach ($computer in Get-Content $ipfile) {
		if (test-connection -computername $computer -count 1 -quiet){ 
			clean $computer
		}
		else {
			echo "$computer not found"
		}
	}
	break	
}
else
{
	$computer = Read-Host -Prompt 'Please enter computer name'
	clean
}

$StopWatch.Stop()
$StopWatch.Elapsed.ToString()
cmd /c pause
@echo off
goto temp

:about
Written by David, AKA RiderExMachina, to be a time saving script.

The usual protocol is:
1. Open either "C:\Windows\temp" or "%temp%" and delete all files
2. Open the unopened directory from Step 1 and delete all files
3. Open MSConfig and disable all unwanted Startup items
4. Open an Administrative Command Prompt and run "chkdsk /f"

This script is to make about 5 minutes of work take 1/4 the time.

:temp
echo Deleting System Temporary Files
del C:\Windows\Temp\* /F /S /Q

echo Deleting User Temporary Files
del %temp%\* /F /S /Q
echo Temporary Files Deleted

:msconfig
echo Opening MSConfig for Startup
msconfig

:chkdsk
echo Setting CHKDSK to Run on Reboot
echo (Press [Y] at Prompt)
chkdsk /F

echo Done!
pause
